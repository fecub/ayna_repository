import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import com.yazcub.aynaweatherforecast 1.0

//Window {
Item {
    id: aweatherroot

    width: 640
    height: 450

    property double scale:1.5

    property double weekChartScale: 2.7/scale
//    property double weekChartMarginTopScale: 10 / scale
    property double aweatherTableSpaceScale: 5 / scale// -250
    property double forecasttableSpaceScale: 100 /scale
    property double weather_itemWidthScale: 8  / scale  //1.5
    property double weather_itemHeightScale: 11  / scale   //3

    property double _weather_itemWidth: width / weather_itemWidthScale //200
    property double _weather_itemHeight: height / weather_itemHeightScale // 50


    DataHandler {
        id: _datahandlerForecastWeather
        onParseRequestCompleted: {
            _day1.day            = _datahandlerForecastWeather.jsonobj_default.FiveDayList[0].day
            _day1.weathericon    = getIconName(_datahandlerForecastWeather.jsonobj_default.FiveDayList[0].icon)
            _day1.temperatureMin = _datahandlerForecastWeather.jsonobj_default.FiveDayList[0].min
            _day1.temperatureMax = _datahandlerForecastWeather.jsonobj_default.FiveDayList[0].max

            _day2.day            = _datahandlerForecastWeather.jsonobj_default.FiveDayList[1].day
            _day2.weathericon    = getIconName(_datahandlerForecastWeather.jsonobj_default.FiveDayList[1].icon)
            _day2.temperatureMin = _datahandlerForecastWeather.jsonobj_default.FiveDayList[1].min
            _day2.temperatureMax = _datahandlerForecastWeather.jsonobj_default.FiveDayList[1].max

            _day3.day            = _datahandlerForecastWeather.jsonobj_default.FiveDayList[2].day
            _day3.weathericon    = getIconName(_datahandlerForecastWeather.jsonobj_default.FiveDayList[2].icon)
            _day3.temperatureMin = _datahandlerForecastWeather.jsonobj_default.FiveDayList[2].min
            _day3.temperatureMax = _datahandlerForecastWeather.jsonobj_default.FiveDayList[2].max

            _day4.day            = _datahandlerForecastWeather.jsonobj_default.FiveDayList[3].day
            _day4.weathericon    = getIconName(_datahandlerForecastWeather.jsonobj_default.FiveDayList[3].icon)
            _day4.temperatureMin = _datahandlerForecastWeather.jsonobj_default.FiveDayList[3].min
            _day4.temperatureMax = _datahandlerForecastWeather.jsonobj_default.FiveDayList[3].max


            _day5.day            = _datahandlerForecastWeather.jsonobj_default.FiveDayList[4].day
            _day5.weathericon    = getIconName(_datahandlerForecastWeather.jsonobj_default.FiveDayList[4].icon)
            _day5.temperatureMin = _datahandlerForecastWeather.jsonobj_default.FiveDayList[4].min
            _day5.temperatureMax = _datahandlerForecastWeather.jsonobj_default.FiveDayList[4].max

            if (!_day5) {
                _day5.day            = _datahandlerForecastWeather.jsonobj_default.FiveDayList[4].day
                _day5.weathericon    = getIconName(_datahandlerForecastWeather.jsonobj_default.FiveDayList[4].icon)
                _day5.temperatureMin = _datahandlerForecastWeather.jsonobj_default.FiveDayList[4].min
                _day5.temperatureMax = _datahandlerForecastWeather.jsonobj_default.FiveDayList[4].max
            }

            _weekChart.parseWeatherData(_datahandlerForecastWeather.jsonobj_default)
        }
    }

    //! [MainView]
    ColumnLayout {
        anchors.fill: parent
        width: parent.width
        height: parent.height
        spacing: (parent.width / aweatherTableSpaceScale) * -1

        WeekChart {
            id: _weekChart
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.fillWidth: true
            Layout.preferredHeight: parent.height / weekChartScale
//            Layout.topMargin: (parent.height / weekChartMarginTopScale) * -1 //-40
        }

        RowLayout {
            id: _forecasttable
            Layout.alignment: Qt.AlignTop | Qt.AlignCenter
            Layout.fillWidth: true
            spacing: aweatherroot.width / forecasttableSpaceScale
            WeatherItem {
                id: _day1
                Layout.preferredWidth: _weather_itemWidth
                Layout.preferredHeight: _weather_itemHeight
            }
            WeatherItem {
                id: _day2
                Layout.preferredWidth: _weather_itemWidth
                Layout.preferredHeight: _weather_itemHeight
            }
            WeatherItem {
                id: _day3
                Layout.preferredWidth: _weather_itemWidth
                Layout.preferredHeight: _weather_itemHeight
            }
            WeatherItem {
                id: _day4
                Layout.preferredWidth: _weather_itemWidth
                Layout.preferredHeight: _weather_itemHeight
            }
            WeatherItem {
                id: _day5
                Layout.preferredWidth: _weather_itemWidth
                Layout.preferredHeight: _weather_itemHeight
            }
        }


    }
    //! [MainView]

    //! TIMER FUNCTION
    Timer {
        interval: 2000
        running: true
        repeat: true

        onTriggered: {
            //! do request
            _datahandlerForecastWeather.doRequest()
        }
    }

    Component.onCompleted: {
        _datahandlerForecastWeather.init(2)
    }


    function getIconName(iconname) {
        //! Weather icon setting
        if(iconname === "01d")
           return "qrc:/images/017-sunny-day.png"

        if(iconname === "01n")
           return "qrc:/images/007-night.png"

        if(iconname === "02d" 	|| iconname === "02n")
           return "qrc:/images/009-partialy-cloudy.png"

        if(iconname === "03d" 	|| iconname === "03n")
           return "qrc:/images/012-cloudy-day.png"

        if(iconname === "04d" 	|| iconname === "04n")
           return "qrc:/images/008-overcast-day.png"

        if(iconname === "09d" 	|| iconname === "09n")
           return "qrc:/images/016-rainy-day.png"

        if(iconname === "10d" 	|| iconname === "10n")
           return "qrc:/images/013-drizzle.png"

        if(iconname === "11d" 	|| iconname === "11n")
           return "qrc:/images/004-lighting.png"

        if(iconname === "13d" 	|| iconname === "13n")
           return "qrc:/images/014-cold.png"

        if(iconname === "50d" 	|| iconname === "50n")
           return "qrc:/images/006-mist.png"
    }
}
