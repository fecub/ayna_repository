import QtQuick 2.7
import QtCharts 2.2
import QtQuick.Layouts 1.1

Item {
    scale: 1
    property double chartViewLeftMarginScale: 50 / scale
    property double barCategoriesAxisFontScale: 50 / scale
    property double valueAxisTitelFontScale: 50 / scale
    property double valueAxisLabelsFontScale: 60 / scale

    ChartView {
        id: chartView
        height: parent.height / 4 * 5
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: -(parent.width/chartViewLeftMarginScale)
        anchors.right: parent.right
        legend.alignment: Qt.AlignTop
        antialiasing: true
        animationOptions: ChartView.SeriesAnimations
        theme: chartviewcolor===1 ? ChartView.ChartThemeLight:ChartView.ChartThemeDark
        backgroundColor: backgroundcolor

        BarCategoryAxis {
            id: barCategoriesAxis
            titleText: ""
            labelsFont.pixelSize: parent.width / barCategoriesAxisFontScale
        }

        ValueAxis {
            id: valueAxisX
            // Hide the value axis; it is only used to map the line series to bar categories axis
            visible: false
            min: 0
            max: 5
        }

        ValueAxis{
            id: valueAxisY
            min: 20
            max: 20
            titleText: "Temp. [&deg;C]"
            titleFont.pixelSize: parent.width / valueAxisTitelFontScale
            labelsFont.pixelSize: parent.width / valueAxisLabelsFontScale
        }

        LineSeries {
            id: maxTempSeries
            axisX: valueAxisX
            axisY: valueAxisY
            name: "Max. temperature"
        }

        LineSeries {
            id: minTempSeries
            axisX: valueAxisX
            axisY: valueAxisY
            name: "Min. temperature"
        }
        BarSeries {
            id: myBarSeries
            axisX: barCategoriesAxis

        }
    }

    function parseWeatherData(weatherData) {
//         Clear previous values
        maxTempSeries.clear();
        minTempSeries.clear();


        // Loop through the parsed JSON
        for (var i in weatherData.FiveDayList) {
            var weatherObj = weatherData.FiveDayList[i];

            // Store temperature values, rainfall and weather icon.
            // The temperature values begin from 0.5 instead of 0.0 to make the start from the
            // middle of the rainfall bars. This makes the temperature lines visually better
            // synchronized with the rainfall bars.
            maxTempSeries.append(Number(i) + 0.5, weatherObj.max);
            minTempSeries.append(Number(i) + 0.5, weatherObj.min);
//            rainfallSet.append(i, weatherObj.precipMM);
//            weatherImageModel.append({"imageSource":weatherObj.weatherIconUrl[0].value});

            // Update scale of the chart
            valueAxisY.max = Math.max(chartView.axisY().max,parseInt(weatherObj.max) + 5);
            valueAxisY.min = Math.min(chartView.axisY().min,parseInt(weatherObj.min) - 10);
//            valueAxisX.min = 0;
            valueAxisX.max = Number(i) + 1;

            // Set the x-axis labels to the dates of the forecast
            var xLabels = barCategoriesAxis.categories;
            xLabels[Number(i)] = weatherObj.day;
            barCategoriesAxis.categories = xLabels;
            barCategoriesAxis.visible = true;
            barCategoriesAxis.min = 0;
            barCategoriesAxis.max = xLabels.length - 1;
        }
    }
}
