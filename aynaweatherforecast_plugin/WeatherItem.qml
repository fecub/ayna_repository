import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

Item {
    id: weatheritemroot

    property double scale: 0.8
    property alias day: _day.text
    property alias weathericon: _weathericon.source
    property alias temperatureMin: _temperatureMin.text
    property alias temperatureMax: _temperatureMax.text
    property double weathericonScale: (weatheritemroot.width / 150) / scale // 0.5
    property double temperatureMinMaxIconScale:  (weatheritemroot.width / 20) / scale

    property double textScale: 7



    ColumnLayout {
        anchors.fill: parent
        spacing: weatheritemroot.width / 30

        Image {
            id: _weathericon
            Layout.alignment: Qt.AlignCenter
            source: "qrc:/images/017-sunny-day.png"
            sourceSize.width: weatheritemroot.width/weathericonScale
            sourceSize.height: weatheritemroot.height/weathericonScale
            ColorOverlay {
                anchors.fill: _weathericon
                source: _weathericon
                color: fontcolor
            }

        }
        Text {
            id: _day
            text: "Montag"
            Layout.fillWidth: true
            horizontalAlignment : Text.AlignHCenter
            font.pixelSize:weatheritemroot.width / textScale
            color: fontcolor
        }

        RowLayout {
            Layout.fillWidth: false
            Layout.alignment: Qt.AlignCenter
            spacing: weatheritemroot.width / 20

            RowLayout{
                Image {
                    id: _maxIcon
                    source: "qrc:/images/caret-arrow-up.png"
                    sourceSize.width: weatheritemroot.width/temperatureMinMaxIconScale //50
                    sourceSize.height: weatheritemroot.height/temperatureMinMaxIconScale //50
                    ColorOverlay {
                        anchors.fill: _maxIcon
                        source: _maxIcon
                        color: fontcolor
                    }
                }

                Text {
                    id: _temperatureMax
                    text: "21°"
                    Layout.fillWidth: true
                    horizontalAlignment : Text.AlignHCenter
                    font.pixelSize:weatheritemroot.width / textScale
                    color: fontcolor
                }
            }

            RowLayout {
                Image {
                    id: _minIcon
                    source: "qrc:/images/caret-arrow-down.png"
                    sourceSize.width: weatheritemroot.width/temperatureMinMaxIconScale //50
                    sourceSize.height: weatheritemroot.height/temperatureMinMaxIconScale //50
                    ColorOverlay {
                        anchors.fill: _minIcon
                        source: _minIcon
                        color: fontcolor
//                        color: "#FFFFFF"
//                        color: "#FFFFFFFF"
                    }
                }
                Text {
                    id: _temperatureMin
                    text: "21°"
                    Layout.fillWidth: true
                    horizontalAlignment : Text.AlignHCenter
                    font.pixelSize:weatheritemroot.width / textScale
                    color: fontcolor
                }

            }


        }
    }
}
