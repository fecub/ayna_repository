import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import com.yazcub.aynanews 1.0

Item {
    id: root

    visible: true
    width: 640
    height: 450

    property double newsTitleFontScale: 20
    property double newsDescriptionFontScale: 40

    FontLoader { id: fontThin; source: "qrc:/font/Lato-Hairline.ttf" }

    DataHandler {
        id: _datahandlernews
        onParseRequestCompleted: {
            _newsModel.clear()
            var i;
            for (i=0; i < _datahandlernews.jsonobj_default.articles.length; i++)
            {
                _newsModel.append({"title": _datahandlernews.jsonobj_default.articles[i].title,
                                  "description": _datahandlernews.jsonobj_default.articles[i].description})
            }
            _pathRoot.model = _newsModel
        }
        onSettingschanged: {
            _datahandlernews.doRequest()
            stepIndex = 0
            newsRequestTimer.restart()
            newsChangeTimer.restart()
        }
    }

    ListModel {
        id: _newsModel
    }

    property int stepIndex: 0
    PathView {
        id: _pathRoot
        anchors.fill: parent

        delegate: flipCardDelegate
        model: _newsModel

        path: Path {
            startX: root.width/2
            startY: 0

            PathAttribute { name: "itemScale"; value: 0.5; }
            PathAttribute { name: "itemZ"; value: 0 }
            PathAttribute { name: "itemTitleFontSize"; value: root.width / newsTitleFontScale }
            PathAttribute { name: "itemDescriptionFontSize"; value: root.width / newsDescriptionFontScale }
            PathAttribute { name: "textFontColorPos0"; value: 0 }
            PathAttribute { name: "textFontColorPos1"; value: 1 }

            PathLine { x: root.width/2; y: root.height*0.4; }
            PathPercent { value: 0.48 }
            PathLine { x: root.width/2; y: root.height*0.5; }

            PathAttribute { name: "itemScale"; value: 1.0; }
            PathAttribute { name: "itemZ"; value: 100 }
            PathAttribute { name: "itemTitleFontSize"; value: root.width / newsTitleFontScale}
            PathAttribute { name: "itemDescriptionFontSize"; value: root.width / newsDescriptionFontScale }
            PathAttribute { name: "textFontColorPos0"; value: 1 }
            PathAttribute { name: "textFontColorPos1"; value: 1 }

            PathLine { x: root.width/2; y: root.height*0.6; }
            PathPercent { value: 0.52; }
            PathLine { x: root.width/2; y: root.height; }

            PathAttribute { name: "itemScale"; value: 0.5; }
            PathAttribute { name: "itemZ"; value: 0 }
            PathAttribute { name: "itemTitleFontSize"; value: root.width / newsTitleFontScale }
            PathAttribute { name: "itemDescriptionFontSize"; value: root.width / newsDescriptionFontScale }
            PathAttribute { name: "textFontColorPos0"; value: 1 }
            PathAttribute { name: "textFontColorPos1"; value: 0 }
        }

        pathItemCount: 3


        preferredHighlightBegin: 0.5
        preferredHighlightEnd: 0.5
    }

    Component {
        id: flipCardDelegate

        Rectangle {
            id: wrapper

            property double textTitleFontSize: PathView.itemTitleFontSize
            property double textDescriptionFontSize: PathView.itemDescriptionFontSize
            property double textFontColorPos0: PathView.textFontColorPos0
            property double textFontColorPos1: PathView.textFontColorPos1

            width: parent.width / 3
            height: parent.height / 3
            antialiasing: true
            color: backgroundcolor

            Text {
                id:_textTitle
                anchors.centerIn: parent
                text: title
                font.pixelSize: textTitleFontSize
                color: fontcoloropposite
                width: root.width / 1.1
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                font.family: fontThin.name
                font.weight: Font.Light
            }

            Text {
                id:_textDescription
                anchors.top: _textTitle.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                width: root.width / 1.5
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: description
                font.pixelSize: textDescriptionFontSize
                color: fontcoloropposite
                font.family: fontThin.name
                font.weight: Font.Light
            }

            LinearGradient  {
                anchors.fill: _textTitle
                source: _textTitle
                gradient: Gradient {
                    GradientStop { position: 0.4; color: textFontColorPos0 === 1 ? fontcolor:fontcoloropposite }
                    GradientStop { position: 1; color: textFontColorPos1 === 1 ? fontcolor:fontcoloropposite }
                }
            }
            LinearGradient  {
                anchors.fill: _textDescription
                source: _textDescription
                gradient: Gradient {
                    GradientStop { position: 0.3; color: textFontColorPos0 === 1 ? "gray":fontcoloropposite }
                    GradientStop { position: 1; color: textFontColorPos1 === 1 ? "gray":fontcoloropposite }
                }
            }

            visible: PathView.onPath

            scale: PathView.itemScale
//            z: PathView.itemZ
        }
    }

    Timer {
        id: newsRequestTimer
        interval: 100000
        running: true
        repeat: true

        onTriggered: {
            //! do request
            _datahandlernews.doRequest()
        }
    }

    //! [TIMER FUNCTION]
    Timer {
        id: newsChangeTimer
        interval: 10000
        running: true
        repeat: true

        onTriggered: {
            stepIndex = stepIndex + 1
            if (stepIndex == _pathRoot.count)
                stepIndex = 0
            _pathRoot.currentIndex = stepIndex
        }
    }
    //! [TIMER FUNCTION]
    Component.onCompleted: {
        _datahandlernews.init(3)
    }
}

