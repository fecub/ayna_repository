import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.1
import QtQuick.Window 2.2

Item {
    id: timeroot
    //    width: parent.width
    //    height: parent.height
    visible: true
    width: 1920
    height: 152

    FontLoader {
        id: fontBold
        source: "qrc:/font/Exo-ExtraLight.otf"
    }
    FontLoader {
        id: fontThin
        source: "qrc:/font/Lato-Hairline.ttf"
    }

    property double dayDateScale: 15 / scale
    property double timeScale: 4 / scale
    property double scale: 0.15
    property double lineScale: 15 / scale

    RowLayout {
        id: dateLayout
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 15
        Text {
            id: dayString
            color: fontcolorlight
            font {
                family: fontBold.name
                pixelSize: timeroot.width / dayDateScale
                capitalization: Font.AllUppercase
                bold: true
            }
        }

        Text {
            id: dayMonthYear
            color: fontcolor
            font {
                family: fontBold.name
                pixelSize: timeroot.width / dayDateScale
            }
        }
    }

    Text {
        id: time
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: dateLayout.bottom
        color: fontcolor

        font {
            family: fontThin.name
            pixelSize: parent.width / timeScale
            weight: Font.Thin
        }
    }

    Rectangle {
        id: line
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        color: linecolor
        height: parent.height / lineScale
        width: parent.width
    }

    //! TIMER FUNCTION
    Timer {
        interval: 500
        running: true
        repeat: true

        onTriggered: {
            var date = new Date()

            dayString.text = date.toLocaleDateString(Qt.locale("de_DE"),
                                                     "dddd,")
            dayMonthYear.text = date.toLocaleDateString(Qt.locale("de_DE"),
                                                        "dd. MMMM yyyy")
            time.text = date.toLocaleTimeString(Qt.locale("de_DE"), "hh:mm:ss")
        }
    }
}
