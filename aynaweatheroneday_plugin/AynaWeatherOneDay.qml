import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQuick.Window 2.2
import com.yazcub.aynaweatheroneday 1.0

Item {
    id: weathercurrentroot

    visible: true
    width: 640
    height: 450

    //    property double scale: 0.8
    property double scale: 0.55

    property double weathericonScale: 1.8 / scale
    property double descriptionScale: 15 / scale
    property double detailFontScale: 25 / scale
    property double temperaturScale: 4 / scale
    property double temperatureMinMaxScale: 14 / scale
    property double temperatureMinMaxIconScale: 15 / scale

    FontLoader {
        id: fontThin
        source: "qrc:/font/Lato-Hairline.ttf"
    }
    FontLoader {
        id: fontBold
        source: "qrc:/font/Exo-ExtraLight.otf"
    }

    /* Summary: Current Weather handler */
    DataHandler {
        id: _datahandlerCurrentWeather
        onParseRequestCompleted: {
            grad.text = _datahandlerCurrentWeather.jsonobj_default.main.temp.toString(
                        ).split(".")[0]
            weather_desc.text = _datahandlerCurrentWeather.jsonobj_default.weather[0].description

            //! Weather icon setting
            if (_datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "01d")
                _weather_icon.source = "qrc:/images/017-sunny-day.png"

            if (_datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "01n")
                _weather_icon.source = "qrc:/images/007-night.png"

            if (_datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "02d" || _datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "02n")
                _weather_icon.source = "qrc:/images/009-partialy-cloudy.png"

            if (_datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "03d" || _datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "03n")
                _weather_icon.source = "qrc:/images/012-cloudy-day.png"

            if (_datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "04d"|| _datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "04n")
                _weather_icon.source = "qrc:/images/008-overcast-day.png"

            if (_datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "09d" || _datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "09n")
                _weather_icon.source = "qrc:/images/016-rainy-day.png"

            if (_datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "10d" || _datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "10n")
                _weather_icon.source = "qrc:/images/013-drizzle.png"

            if (_datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "11d" || _datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "11n")
                _weather_icon.source = "qrc:/images/004-lighting.png"

            if (_datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "13d" || _datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "13n")
                _weather_icon.source = "qrc:/images/014-cold.png"

            if (_datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "50d" || _datahandlerCurrentWeather.jsonobj_default.weather[0].icon.toString() === "50n")
                _weather_icon.source = "qrc:/images/006-mist.png"

            humidity.text = _datahandlerCurrentWeather.jsonobj_default.main.humidity + " %"
            pressure.text = _datahandlerCurrentWeather.jsonobj_default.main.pressure + " hPa"
            wind.text = _datahandlerCurrentWeather.jsonobj_default.wind.speed + " m/s"
            visibility.text = _datahandlerCurrentWeather.jsonobj_default.visibility
            clouds.text = _datahandlerCurrentWeather.jsonobj_default.clouds.all

            min.text = _datahandlerCurrentWeather.jsonobj_default.main.temp_min
            max.text = _datahandlerCurrentWeather.jsonobj_default.main.temp_max
        }
    }

    ColumnLayout {
        anchors.top: parent.top
        anchors.topMargin: parent.width / 40
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: weathercurrentroot.height / 13

        Text {
            id: weather_desc
            text: "Teils Wolkig"
            anchors.horizontalCenter: parent.horizontalCenter
            color: fontcolor
            font {
                family: fontThin.name
                pixelSize: weathercurrentroot.width / descriptionScale
                weight: Font.Light
            }
        }

        Image {
            id: _weather_icon
            sourceSize.width: weathercurrentroot.width / weathericonScale //50
            sourceSize.height: weathercurrentroot.height / weathericonScale //50
            source: "qrc:/images/001-smog.png"
            fillMode: Image.PreserveAspectFit
            smooth: true
            Layout.alignment: Qt.AlignCenter
            ColorOverlay {
                anchors.fill: _weather_icon
                source: _weather_icon
                color: fontcolor
            }
        }

        RowLayout {
            anchors.top: _weather_icon.bottom
            anchors.topMargin: parent.width / 15
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: weathercurrentroot.width / 10

            ColumnLayout {
                Text {
                    id: grad
                    text: "10"
                    Layout.alignment: Qt.AlignLeft
                    color: fontcolor

                    font {
                        family: fontThin.name
                        pixelSize: weathercurrentroot.width / temperaturScale //30
                        weight: Font.Thin
                    }

                    Text {
                        id: celsius
                        text: "°"
                        bottomPadding: parent.width / 12
                        color: fontcolor

                        style: Text.Normal
                        verticalAlignment: Text.AlignTop
                        anchors.left: grad.right

                        font {
                            family: fontThin.name
                            pixelSize: weathercurrentroot.width / temperaturScale //30
                            weight: Font.Thin
                        }
                    }
                }

                RowLayout {
                    Layout.alignment: Qt.AlignCenter
                    spacing: weathercurrentroot.width / 30

                    RowLayout {
                        spacing: 0
                        Image {
                            id: _max_icon
                            source: "qrc:/images/caret-arrow-up.png"
                            sourceSize.width: weathercurrentroot.width / temperatureMinMaxIconScale //50
                            sourceSize.height: weathercurrentroot.height / temperatureMinMaxIconScale //50
                            fillMode: Image.Stretch

                            ColorOverlay {
                                anchors.fill: _max_icon
                                source: _max_icon
                                color: fontcolor
                            }
                        }

                        Text {
                            id: max
                            text: "10°"
                            color: fontcolor
                            font {
                                family: fontThin.name
                                pixelSize: weathercurrentroot.width / temperatureMinMaxScale //30
                                weight: Font.Thin
                            }
                        }
                    }

                    RowLayout {
                        spacing: 0
                        Image {
                            id: _min_icon
                            source: "qrc:/images/caret-arrow-down.png"
                            sourceSize.width: weathercurrentroot.width
                                              / temperatureMinMaxIconScale //50
                            sourceSize.height: weathercurrentroot.height
                                               / temperatureMinMaxIconScale //50
                            ColorOverlay {
                                anchors.fill: _min_icon
                                source: _min_icon
                                color: fontcolor
                            }
                        }
                        Text {
                            id: min
                            text: "10°"
                            color: fontcolor

                            font {
                                family: fontThin.name
                                pixelSize: weathercurrentroot.width / temperatureMinMaxScale //30
                                weight: Font.Thin
                            }
                        }
                    }
                }
            }

            GridLayout {
                columns: 2
                rows: 4
                rowSpacing: weathercurrentroot.height / 60
                columnSpacing: weathercurrentroot.height / 15
                Layout.topMargin: weathercurrentroot.height / 20

                Text {
                    text: "FEUCHTIGKEIT"
                    color: fontcolor
                    font {
                        family: fontThin.name
                        weight: Font.Light
                        pixelSize: weathercurrentroot.width / detailFontScale //30
                    }
                }
                Text {
                    id: humidity
                    text: "10"
                    color: fontcolor
                    font {
                        family: fontThin.name
                        weight: Font.Light
                        pixelSize: weathercurrentroot.width / detailFontScale //30
                    }
                }

                Text {
                    text: "DRUCK"
                    color: fontcolor
                    font {
                        family: fontThin.name
                        weight: Font.Light
                        pixelSize: weathercurrentroot.width / detailFontScale //30
                    }
                }
                Text {
                    id: pressure
                    text: "10"
                    color: fontcolor
                    font {
                        family: fontThin.name
                        weight: Font.Light
                        pixelSize: weathercurrentroot.width / detailFontScale //30
                    }
                }

                Text {
                    text: "WIND"
                    color: fontcolor
                    font {
                        family: fontThin.name
                        weight: Font.Light
                        pixelSize: weathercurrentroot.width / detailFontScale //30
                    }
                }
                Text {
                    id: wind
                    text: "10"
                    color: fontcolor
                    font {
                        family: fontThin.name
                        weight: Font.Light
                        pixelSize: weathercurrentroot.width / detailFontScale //30
                    }
                }

                Text {
                    text: "SICHTBARKEIT"
                    color: fontcolor
                    font {
                        family: fontThin.name
                        weight: Font.Light
                        pixelSize: weathercurrentroot.width / detailFontScale //30
                    }
                }
                Text {
                    id: visibility
                    text: "10"
                    color: fontcolor
                    font {
                        family: fontThin.name
                        weight: Font.Light
                        pixelSize: weathercurrentroot.width / detailFontScale //30
                    }
                }

                Text {
                    text: "WOLKEN"
                    color: fontcolor
                    font {
                        family: fontThin.name
                        weight: Font.Light
                        pixelSize: weathercurrentroot.width / detailFontScale //30
                    }
                }
                Text {
                    id: clouds
                    text: "10"
                    color: fontcolor
                    font {
                        family: fontThin.name
                        weight: Font.Light
                        pixelSize: weathercurrentroot.width / detailFontScale //30
                    }
                }
            }
        }
    }

    Timer {
        interval: 2000
        running: true
        repeat: true

        onTriggered: {
            //! do request
            _datahandlerCurrentWeather.doRequest()
        }
    }

    Component.onCompleted: {
        _datahandlerCurrentWeather.init(1)
    }
}
